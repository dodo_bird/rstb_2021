# rstb_2021

The code (R and Matlab) and data archives here complement the paper
"Cross-frequency coupling explains the preference for simple ratios in rhythmic
% behaviour and the relative stability across non-synchronous patterns"
by Dotov & Trainor (2021).

We use the phase-attractive circle map to explore whether a model consistent with 
cross-frequency coupling could potentially account for certain tendencies in 
bimanual tapping, and very superficially in iterated rhythm tapping.

Tapping data + scripts to reproduce figures and stats is in the Section 2 folder. The scripts are named to indicate which figures they generate.

Listening data + scripts to reproduce figures and some of the stats is in the Section 3 folder.

The modeling part is in Section 4. For bimanual tapping-like effects, run MASTER_CIRCLE_MAP_AS_TAPPING_PHASE.m. It will generate three figures (Figure 4, Supplementary Figure 5, and Supplementary Figure 8).

For iterated rhythm tapping-like effects, run MASTER_CIRCLE_MAP_FOR_ITERATED_TAPPING.m. 
It will generate Figure 5.

All dependencies should be included. You could use a toolbox for better 
fitting the slope  of the scaling law but it is not necessary. For more details, 
look inside the MASTER... scripts as well as the article itself: 
https://...

Dobromir Dotov, 2021
