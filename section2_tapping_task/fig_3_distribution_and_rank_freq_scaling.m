function fig_3_distribution_and_rank_freq_scaling()
% Run this to show how the distribution and rank-frequency scaling
% of tapping phases, pooled across all trials and participants.
% Several required functions are included, save for one fitting toolbox.
% This reproduces Figure 3 from the paper "Cross-frequency coupling
% explains the preference for simple ratios in rhythmic
% behaviour and the relative stability across 
% non-synchronous patterns", Dotov and Trainor.

load('data_tapping_phases.mat','PQD');

FS = make_farey_seq(45);
FS.table = FS.table(FS.table(:,4)<=pi,:); % limit w/in [0 pi]
FS.table(:,6) = FS.table(:,2)./FS.table(:,3)*360; % phi in degrees
xticks = FS.table(FS.table(:,5)<=8,:); % sparser set of phases to indicate on the figure x-axis.

% Locations of histogram bins. The phases tend to cluster very narrowly,
% in particular around the Farey tree ratios. These are isolated points,
% and are ordered weirdly along the real line, so the clusters are missed
% when using wide bins with arbitrarily defined centers.
% Start with theoretically defined bin centers, then fill the space around.
tbins = FS.table(:,6)';
db = 1; % 1 degree bin width
bins = tbins(1)-db;
for i=2:numel(tbins)
    bins = [bins tbins(i-1):db:(tbins(i))];
end
bins = [bins tbins(end) tbins(end)+db];
bins = unique(bins);

display_rank_scaling(PQD.all_tapping_phases,bins,xticks(:,6),xticks(:,2),xticks(:,3),xticks(:,5));
% print('-djpeg','-r100','tapping_phase_distributions.jpg')

end


function display_rank_scaling(THETAS,bins,xticks,p,q,l)

if false
    [alw,fsz,~,~] = pretty_canvas([4 3]*1.5); % changes figure defaults.
else
    clf
    alw=1;
    fsz=12;
end

if isempty(bins);bins = 1e2;end

[c,n]=hist(THETAS,bins); % the histogram
c=c./sum(c);
thetahist = [n' c'];
thetahist = sortrows(thetahist,1,'descend'); % sort it

% Main plot, the distribution. In two colours.
plot(thetahist(:,1),thetahist(:,2),'-','LineWidth',3,'color',[.1 .1 .1])
hold on
plot(thetahist(:,1),thetahist(:,2),'s','LineWidth',.1,'color',[.1 .1 .1],...
    'MarkerSize',2,'MarkerFaceColor',[.8 .5 1],'MarkerEdgeColor',[.8 .5 1])
hold off
xlabel('\theta, ^o')
ylabel('P(\theta)')
grid on

% Top levels from the Farey tree are shown as points of interest on the x-axis.
if ~isempty(xticks)
    set(gca,'XTick',xticks)
    xlim([min(xticks)-.1 max(xticks)+.1])
end
if ~isempty(p) && ~isempty(q)
    set(gca,'XTickLabel',[num2str(p) repmat(':',size(p,1),1) num2str(q) repmat(', L',size(p,1),1) num2str(l) repmat(', ',size(p,1),1) num2str(round(xticks)) repmat('^o',size(p,1),1)])
else
    set(gca,'XTickLabel',round(xticks))
end
xtickangle(40)

set(gca,'fontsize',fsz)
set(gca,'linewidth',alw)

% Prepare for rank-frequency scaling.
thetahist = sortrows(thetahist,2,'descend');
thetahistwithoutthezeros = thetahist(thetahist(:,2)>0,:);

% Only fit the left half of the distribution.
[~,corner] = min(abs((1:size(thetahistwithoutthezeros,1)) - (((size(thetahistwithoutthezeros,1)) - (1))*1/2)));
corner = min([corner size(thetahistwithoutthezeros,1)]); % check that it's not too large.
ranks = (1:corner)';

v = ver; % You might not have this toolbox.
if any(strcmp ('Curve Fitting Toolbox', {v.Name}))
    % The Zipf-Mandelbrot rank scaling law
    zml=@(c,q,s,x)(log10(c)+s*log10(x+q));
    
    % Fitting params and run.
    fo = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',[0, -.1, -1e1],...
        'Upper',[1e1, 1e2, 1e1],...
        'StartPoint',[.1, .1, 0]);
    ft = fittype(zml,'options',fo);
    [zm_model_fitted,~] = fit(ranks,log10(thetahistwithoutthezeros(ranks,2)),ft);
    
    % The fitted curve.
    fitted_scaling_law = zml(zm_model_fitted.c,zm_model_fitted.q,zm_model_fitted.s,ranks);
    r2 = corr(fitted_scaling_law,log10(thetahistwithoutthezeros(ranks,2)))^2;
else
    fprintf('\n¡The needed toolbox was not found!\nNot bothering with fitting the full Zipf-Mandelbrot, just the classic scaling law.\n\n')
    b = [thetahistwithoutthezeros(ranks,2).^0 log10(ranks)]\(log10(thetahistwithoutthezeros(ranks,2)));
    
    % The fitted curve.
    fitted_scaling_law = [thetahistwithoutthezeros(ranks,2).^0 log10(ranks)]*b;
    r2 = corr(log10(thetahistwithoutthezeros(ranks,2)), fitted_scaling_law)^2;
end

% Inset inside the main figure.
axes('Position',[.56 .6 .37 .35],'units','normalized')

% The rank-freq distribution in log-log.
loglog(1:size(thetahist,1),thetahist(:,2),'s','LineWidth',1,'color',[.1 .3 .1],...
    'MarkerSize',5,'MarkerFaceColor',[.8 .5 1])
hold on
loglog(ranks,10.^(fitted_scaling_law),'-.','color',[.1 .85 .4],'LineWidth',3)
hold off
box off

set(gca, 'Color', 'none')

if any(strcmp ('Curve Fitting Toolbox', {v.Name}))
    text(.3,1.,['$$\tilde P_{Z-M} \propto (k+q)^{' num2str(zm_model_fitted.s,2) '}$$'],'units','normalized','interpreter','latex','fontsize',fsz+5)
    fprintf('P ~ c(rank+q)^s, (c=%.3f, q=%.3f, s=%.3f, R^2=%.3f)\n', zm_model_fitted.c, zm_model_fitted.q, zm_model_fitted.s, r2)
else
    text(.3,1.,['$$\tilde P_{Z} \propto (k)^{' num2str(b(2),2) '}$$'],'units','normalized','interpreter','latex','fontsize',fsz+5)
    fprintf('P ~ c(rank)^s, (c=%.3f, s=%.3f, R^2=%.3f)\n', b(1), b(2), r2)
end

xlabel('Rank')
ylabel('P(\theta)')
set(gca,'linewidth',alw)
set(gca,'fontsize',fsz)

end


function [alw,fsz,lw,msz] = pretty_canvas(dims)

if isempty(dims)
    width = 4; % "
    height= 3; % "
else
    width = dims(1);
    height= dims(2);
end

alw = .75;    % AxesLineWidth
fsz = 10;      % Fontsize
lw = 2;      % LineWidth
msz = 7;       % MarkerSize

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% The properties we've been using in the figures
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz

% Set the default Size for display
defpos = get(0,'defaultFigurePosition');
set(0,'defaultFigurePosition', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','inches'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);

end


function FT = make_farey_seq(n)
%MAKE_FAREY_SEQ    A table with p:q ratios following the Farey sequence
% Indicate how many levels down the tree you want to go.
%
% Dobromir Dotov, 2021

ftree=[1 0 1 0 1;2 1 1 2*pi 1];
c=2;
for j=2:n
    for p=1:j
        for q=2:j
            if ~any((p/q)==(ftree(:,2)./ftree(:,3))) && p/q<1
                c=c+1;
                ftree=[ftree;[c p q p/q*2*pi j]];
            end
        end
    end
end
[~,index]=sort(ftree(:,end-1));
ftree=ftree(index,:);

FT.labels=[{'n'},'p','q','theta_rad','order'];
FT.table=ftree;

end
