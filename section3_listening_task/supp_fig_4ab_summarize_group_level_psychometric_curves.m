clear;close all

% This reproduces Supplementary Figures 4a and 4b from the paper 
% "Cross-frequency coupling explains the preference for simple ratios in 
% rhythmic behaviour and the relative stability across non-synchronous 
% patterns", Dotov and Trainor.
%
% Run this to show rough psychophysics of a yes/no auditory deviation task.
% The task consists of detecting a change in the time interval between
% two auditory stimuli. The two beeps repeat cyclically four times,
% separated by a given phase. In the last cycle the phase may or may not
% change. We were interested in how this detection depends on the initial
% phase, where the phase is expressed as a cycle ratio and this ratio is
% mapped to the Farey tree which gives a metric of ratio complexity.
%
% Two cohorts of participants correspond to two almost identical iterations
% of the study, with slight changes of the number, range, and repetition of
% stimuli. The first cohort was collected in the spring of 2020, the second
% in the fall/winter of 2020.

load('data_2020-04-27.mat','DATA');DATA1 = DATA;clear DATA
% Behavior was strange for stimuli<90 deg in combination with deviations, 
% probably because the beeps start to coincide at these phase and deviations.
DATA1(DATA1.phi<90,:) = [];

% This function fits a sigmoidal function to yes/no responses ~ tau, 
% separately by stimulus pattern (phase) and positive/negative deviations.
% Important. Keep in mind that here we only deal with group-level fitting,
% proportion across all participants, which is not the usual way of doing psychophysics.
[S,HITS,~,~,~,~] = psymetri_across_pp_per_phi(DATA1, 0);

figure(1)
plot_psymetri_summary(table2array(HITS),table2array(S),'level')
% print('-djpeg','-r100','aud_task_group_level_params_cohort1.jpg')


clear
load('data_2021-02-11.mat','DATA');DATA2 = DATA;clear DATA
[S,HITS,~,~,~,~] = psymetri_across_pp_per_phi(DATA2, 0);
figure(2)
plot_psymetri_summary(table2array(HITS),table2array(S),'level')
% print('-djpeg','-r100','aud_task_group_level_params_cohort2.jpg')
