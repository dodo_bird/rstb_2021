function plot_psymetri_summary(HITS,S,order_by)

% What's on the x-axis.
if strcmp(order_by,'level')
    label_x = 'Ratio Complexity';
else
    label_x = 'Phi, deg';
end

% Fig defaults that are nicer for printing.
if true
    [alw,lw,ms,fsz] = nice_fig_params();
else
    clf
    alw=1;lw=2;ms=10;fsz=8;
end

% First show correct~deviation (tau).
subplot(1,3,1)

% for clarity, remove the deviation = 0 ms trials.
HITS(HITS(:,1)==0,6)=nan;

% range of stimulus properties.
hits_iv = unique(HITS(:,5));
ivs = [];
for i = 1:numel(hits_iv)
    ivs(i,:) = HITS(find(HITS(:,5)==hits_iv(i),1,'first'),2:5);
end

% for matching colors, sort the iv for the x-axis.
if strcmp(order_by,'level')
    ivs = sortrows(ivs,2);
else
    ivs = sortrows(ivs,4);
end

% do the plotting in a loop by iv level.
label_legend = [];
color_vec = copper(size(ivs,1));
for l = 1:size(ivs,1)
    hits = HITS(HITS(:,5)==ivs(l,4),:);
    label_legend{l} = [num2str(ivs(l,1)) ':' num2str(ivs(l,2))];
    p(l) = plot(hits(:,1),hits(:,6)*100,'-','linewidth',lw);
    set(p(l),'color',color_vec(l,:))
    hold on
end
hold off
ylabel('Correct, %')
xlabel('Deviation, ms')
legend(p(1:2:end),label_legend(1:2:end),'location','north')
set(gca,'linewidth',alw)
text(.06,.9,'A','unit','normalized')


% show the thresholds.

% the range of stimulus properties.
hits_iv = unique(S(:,4));
ivs = [];
for i = 1:numel(hits_iv)
    ivs(i,:) = S(find(S(:,4)==hits_iv(i), 1, 'first'), 1:4);
end
if strcmp(order_by,'level')
    S = sortrows(S, 3);
    iv_col = 3;
else
    S = sortrows(S, 4);
    iv_col = 4;
end
color_vec = copper(size(ivs,1));

% do the plotting + overall simple regression, loop by positive/negative and stimulus level.
subplot(1,3,2)
for neg=0:1
    s = S(S(:,5)==neg,:);
    for l = 1:size(s,1)
        p(l) = plot(s(l,iv_col),s(l,6),'o','linewidth',lw,'markersize',ms);
        set(p(l),'color',color_vec(l,:),'markerfacecolor',[.4 .4 .4])
        hold on
    end
    b=[s(:,iv_col)*0+1 s(:,iv_col)]\s(:,6);
    plot(s(:,iv_col),[s(:,iv_col)*0+1 s(:,iv_col)]*b,'--k','linewidth',lw)
    ylabel('\theta, ms')
    xlabel(label_x)
    ylim([-150 150])
    xlim([min(s(:,iv_col))*.9 max(s(:,iv_col))*1.05])
    set(gca,'linewidth',alw)
    set(gca,'fontsize',fsz)
    text(.06,.9,'B','unit','normalized')
end

% now show sigma.
% do the plotting + overall simple regression, loop by positive/negative and stimulus level.
subplot(1,3,3)
for neg=0:1
    s = S(S(:,5)==neg,:);
    for l = 1:size(s,1)
        if neg==0
            p(l) = plot(s(l,iv_col),s(l,7),'<','linewidth',lw,'markersize',ms);
        else
            p(l) = plot(s(l,iv_col),s(l,7),'>','linewidth',lw,'markersize',ms);
        end
        set(p(l),'color',color_vec(l,:),'markerfacecolor',[.4 .4 .4])
        hold on
    end
    b=[s(:,iv_col)*0+1 s(:,iv_col)]\s(:,7);
    plot(s(:,iv_col),[s(:,iv_col)*0+1 s(:,iv_col)]*b,'--k','linewidth',lw)

    ylabel('\sigma')
    xlabel(label_x)
    xlim([min(s(:,iv_col))*.9 max(s(:,iv_col))*1.05])
    set(gca,'linewidth',alw)
    xlabel(label_x)
    ylim([-.06 .06])
    text(.07,.9,'C','unit','normalized')
end

end

function [alw,lw,msz,fsz] = nice_fig_params()

% Canvas parameters
width = 12; % "
height = 3; % "
alw = .75; % AxesLineWidth
fsz = 8;   % Fontsize
lw = 2.;   % LineWidth
msz = 10;  % MarkerSize

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
% close all;
 
% The properties we've been using in the figures
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(gcf,'color','w')

% Set the default Size for display
defpos = get(0,'defaultFigurePosition');
set(0,'defaultFigurePosition', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','inches'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);

end