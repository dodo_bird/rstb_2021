function [S,HITS,levels,phis,betasout,rtvec] = psymetri_across_pp_per_phi(DATA, plotting_flag)

phis = unique(DATA.phi);
betasout = [];
S = [];
rtvec = [];
mseout = [];
levels = [];

for neg=0:1
    for p = 1:numel(phis)
        % break up the data by stimulus and positive/negative deviations.
        index = find(logical((DATA.phi==phis(p)).*((DATA.tau*(neg*2-1))>=0)));

        y = DATA.x(index);
        x = DATA.tau(index);
        
        % also collect reaction times for a separate analysis.
        rt = DATA.rt(index);
        rtvec(p,:,neg+1) = [mean(rt) std(rt)./numel(rt)^.5];
        levels(p,1) = DATA.l(find(DATA.phi==phis(p),1,'first'));
        
        % yes/no response (not correct!).
        taus = unique(x);
        prop = nan(size(taus));
        for t = 1:numel(taus)
            prop(t) = sum(y(x==taus(t)))/sum(x==taus(t));
        end
        
        % the fitting is done here.
        [betasout(p,:,neg+1),mseout(p,1,neg+1)] = sigm_fit_and_solve(taus,prop,plotting_flag);
        
        % collect the measures.
        S = [S;[DATA.p(index(1)),DATA.q(index(1)),DATA.l(index(1)),phis(p),neg,betasout(p,:,neg+1),mseout(p,1,neg+1)]];
    end
end
S = array2table(S);
S.Properties.VariableNames = {'p','q','l','phi','neg','theta','sigma','mse'};

% count percent correct responses.
HITS = [];
phis = unique(DATA.phi);
taus = unique(DATA.tau);
for p = 1:numel(phis)
    for t = 1:numel(taus)
        index = find(logical((DATA.phi==phis(p)).*((DATA.tau)==taus(t))));
        hits = sum(DATA.correct(index))/numel(index);
        HITS = [HITS;[taus(t),DATA.p(index(1)),DATA.q(index(1)),DATA.l(index(1)),phis(p),hits]];
    end
end
HITS = array2table(HITS);
HITS.Properties.VariableNames = {'tau','p','q','l','phi','hits'};

end

function [betas,mseout] = sigm_fit_and_solve(x,y,plotting_flag)
init = [60 .05]*((mean(x)>0)*2-1);
sigmoidal = @(p,x)(exp((x-p(1))*p(2))./(exp((x-p(1))*p(2)) + 1));
warning('off','stats:nlinfit:ModelConstantWRTParam')
[betas,~,~,~,mseout]=nlinfit(x,y,sigmoidal,init,statset('MaxIter',1e3));
yhat=sigmoidal(betas,x);

if plotting_flag == 1
    plot(x,y,'-or')
    hold on
    plot(x,yhat,'--k')
    hold off
    text(betas(1),.5,num2str(betas(1),'%.3f'))
    text(betas(1),.4,num2str(betas(2),'%.3f'))
    pause
end
end