function FT = make_farey_seq(n)
%MAKE_FAREY_SEQ    A table with p:q ratios following the Farey sequence
% Indicate how many levels down the tree you want to go.
%
% Dobromir Dotov, 2021

ftree=[1 0 1 0 1;2 1 1 2*pi 1];
c=2;
for j=2:n
    for p=1:j
        for q=2:j
            if ~any((p/q)==(ftree(:,2)./ftree(:,3))) && p/q<1
                c=c+1;
                ftree=[ftree;[c p q p/q*2*pi j]];
            end
        end
    end
end
[~,index]=sort(ftree(:,end-1));
ftree=ftree(index,:);

FT.labels=[{'n'},'p','q','theta_rad','order'];
FT.table=ftree;
