function SB=make_stern_brocot_tree(maxlevel)
%MAKE_STERN_BROCOT_TREE     A table with p:q ratios following Stern-Brocot
% Indicate how many levels down the tree you want to go. Not very efficient
% so don't go crazy with the levels. Above l=18 this is impractical.
%
% Dobromir Dotov, 2021

if isempty(maxlevel)
    maxlevel=5;
end
sb=[1,0,1,0,1,1;
    2,1,1,2*pi,1,1];
c=size(sb,1);
for order=2:maxlevel
    num=sb(:,2)./sb(:,3);
    [~,in]=sort(num,'ascend');
    temp=[];
    for n=2:numel(in)
        c=c+1;
        temp=vertcat(temp,...
            [c sb(in(n-1),2)+sb(in(n),2) sb(in(n-1),3)+sb(in(n),3) ...
            (sb(in(n-1),2)+sb(in(n),2))/(sb(in(n-1),3)+sb(in(n),3))*2*pi ...
            order max(sb(in(n-1:n),1))]);
    end
    sb=vertcat(sb,temp);
end

SB.labels=[{'n'},'p','q','theta_rad','level','parent'];
SB.table=sortrows(sb,4);