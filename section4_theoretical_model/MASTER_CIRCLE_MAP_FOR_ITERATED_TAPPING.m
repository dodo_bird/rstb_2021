function MASTER_CIRCLE_MAP_FOR_ITERATED_TAPPING()
%MASTER_CIRCLE_MAP_FOR_ITERATED_TAPPING
%   Run MASTER_CIRCLE_MAP_FOR_ITERATED_TAPPING to generate the
%   simplex-like figure (Figure 5 in Dotov & Trainor, 2021). No dependencies, 
%   this script must be all that's needed.
%   
%   This runs a very basic simulation. Three independent circle maps are
%   iterated separately for repetitions*iterations number of steps. The
%   first set of omegas is random. Then, the final winding numbers at the
%   end of an iteration are re-used as omegas in the next iteration. This
%   mimics a very popular paradigm (Jacobi et al. 2017).
%   
%   The winding numbers w are collected. These stand for how much of the 
%   unit cycle is traversed in one step. The idea is that the w correspond 
%   to a parameter that controls the timing of intervals in a rhythm. There 
%   are indications in the literature that parameters of neural oscillations 
%   such as their amplitude or speed are related to timing of behavioral events.
%   
%   Next, the three maps are concatenated each iteration step to make one 
%   execution of the rhythm. Here we model some tendencies in 
%   three-interval rhythms, so we run three circle maps and concatenate them together.
%   
%   Note that the three circle maps are independent of each other in 
%   present script which is a departure from human tapping which
%   exhibits serial dependencies even if you leave aside musical priors
%   associated with tapping more complex rhythms.
%   
%   The so-called phase-attractive circle map from deGuzman & Kelso (1991)
%   is used here for consistency with the other part of the paper. Plus
%   some noise. I have not tried the classical circle map, it should be fine too.
%
%   See the function below for the simplex that visualizes the distribution
%   of intervals, expressed as proportions of the total three-interval rhythm.
%   
% Dobromir Dotov, 2021

A = .8; % deGuzman & Kelso (1991) experimented with two coupling params.
K = 1.1;
sigma = .005;

intervals = 3;
iterations = 5;
repetitions = 1e1; % Pattern repetitions inside one iteration.
sims_n = 1e4;

omegas = zeros(iterations,intervals,sims_n);
omega_out = zeros(iterations,intervals,sims_n);
W = zeros(repetitions,intervals,iterations,sims_n);
for s = 1:sims_n
    ratios = randi(100,1,intervals);
    % ratios = ratios./100*intervals; % (0,3], uniform.
    ratios = ratios./sum(ratios).*intervals; % (0,3], mode at 1.
    omegas(1,:,s) = ratios;
    for n = 1:iterations
        for k = 1:intervals
            theta = 0;
            for i = 1:repetitions
                W(i,k,n,s) = omegas(n,k,s) - K/2/pi*(1+A*cos(2*pi*mod(theta,1)))*sin(2*pi*mod(theta,1)) + sigma*randn;
                theta = theta + W(i,k,n,s);
            end
        end
        omega_out(n,:,s) = W(end,:,n,s);
        omegas(n+1,:,s) = omega_out(n,:,s);
        omegas(n+1,omegas(n+1,:,s)<.1,s) = 1; % assume that /omega=0 wraps to 1.
    end
    if mod(s,10)==0;fprintf('%8.2f%% trials,',s/sims_n*1e2);end
    if mod(s,5e1)==0;fprintf('\n');end
end
fprintf('\n')


% Retain only the final iteration. Then show a histogram in two dimensions, 
% after projecting to a particular 2D coordinate frame.
W_last_iteration = permute(squeeze(W(end,:,:,:)),[2 1 3]);
equitri_frame_and_hist(squeeze(W_last_iteration(end,:,:))',1e2)
%set(gcf,'InvertHardcopy','off')
%print('-depsc2',['fig_triang_' datestr(now,'yyyy-mm-dd-HHMMSS') '.eps'])
%print('-djpeg','-r300',['fig_triang_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpg'])


% Plot in bulk the trajectories of intervals.
if false
    figure
    color_vec = cool(intervals);
    for s=1:10:sims_n
        for k=1:intervals
            plot(omega_end(:,k,s),'-','linewidth',2,'color',color_vec(k,:)*(s/sims_n))
            hold on
        end
    end
    hold off
    set(gca,'YTick',0:.5:2.5)
    grid on
    xlabel('Iteration')
    ylabel('\omega')
end

end


%% Build a simplex-like histogram.
function equitri_frame_and_hist(X,nbins)
% Convert a 3D matrix (columns of intervals p,q,r) to proportions of total 
% p+q+r duration, project on 2D, take the histogram, then plot along with 
% reference points indicating integer p:q:r ratios. There are more rigorous
% and elegant ways of building the so-called simplex, but this works.
% DD, 2021

% Change some default plotting settings to make a nicer figure.
nice_canvas([4*1.5 3*1.5]);

% After rotations most of the var will be in the 2nd and 3rd dimensions (columns), we'll take the hist there.
%rotx = @(theta)([1 0 0; 0 cos(theta) -sin(theta); 0 sin(theta) cos(theta)]);
roty = @(theta)([cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)]);
rotz = @(theta)([cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1]);
Mz = rotz(-pi/4);
My = roty(pi/5.1);


% Prepare the coordinate system.
% Triangle
triaxes = eye(3); 
triaxes = triaxes*.7; % Trim margins of the simplex for better visibility.
triaxes = (My*Mz*triaxes')';

% Labels for p:q:r = integer ratios such as 1:1:1 or 1:2:2
[p,q,r]=ndgrid(1:3,1:3,1:3);
integerlabels = [reshape(p,[],1) reshape(q,[],1) reshape(r,[],1)];
integerlabels(sum(integerlabels == [2 2 2],2)==3,:) = [];
integerlabels(sum(integerlabels == [3 3 3],2)==3,:) = [];
integerlabellocations = integerlabels./sum(integerlabels,2);
integerlabellocations = (My*Mz*(integerlabellocations)')';


% Project the data, then hist.
X = X./sum(X,2); % Convert to proportion from the full rhythm duration.
Xr = (My*Mz*X')';
Counts = hist3(Xr(:,2:3),'Nbins', nbins*[1 1]);
Counts = Counts./sum(sum(Counts));
Counts = Counts.^.5;
Counts = Counts';

barx = linspace(min(Xr(:,2)),max(Xr(:,2)),size(Counts,2)); % Columns of Counts
bary = linspace(min(Xr(:,3)),max(Xr(:,3)),size(Counts,1)); % Rows of Counts

% Start drawing the coordinate system
clf
line(triaxes([1 2 3 1],2),triaxes([1 2 3 1],3),'linewidth',.75);
hold on

% Plot the bars color-coded as a checkerboard plot, in hot colormap.
h = pcolor(barx,bary,Counts);
colormap('hot')
h.ZData = -ones(size(Counts)); % Push under/behind the frame and labels

% Draw the coordinate system
plot(integerlabellocations(:,2),integerlabellocations(:,3),'+','MarkerSize',10,'LineWidth',2,'Color',[.5 .7 1])
for r=1:size(integerlabellocations,1)
    text(integerlabellocations(r,2)+.01,integerlabellocations(r,3)*1.05,[num2str(integerlabels(r,1)),num2str(integerlabels(r,2)),num2str(integerlabels(r,3))],'fontsize',10,'color',[1 1 1])
end
text(.45,-.02,'Interval I','fontsize',10,'color',[1 1 1],'units','normalized')
text(.9,.5,'Interval II','fontsize',10,'color',[1 1 1],'units','normalized')
text(-.0,.5,'Interval III','fontsize',10,'color',[1 1 1],'units','normalized')

axis off
axis([-.5 .5 -.4 .6]) % Cut the margins
set(gcf,'color',[0 0 0])

hold off

end

function [alw,fsz,lw,msz] = nice_canvas(dims)

if isempty(dims)
    width = 4; % "
    height= 3; % "
else
    width = dims(1);
    height= dims(2);
end

alw = .75;    % AxesLineWidth
fsz = 10;      % Fontsize
lw = 2;      % LineWidth
msz = 7;       % MarkerSize

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% The properties we've been using in the figures
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz

% Set the default Size for display
defpos = get(0,'defaultFigurePosition');
set(0,'defaultFigurePosition', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','inches'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);

end
