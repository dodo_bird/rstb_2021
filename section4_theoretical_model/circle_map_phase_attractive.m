function [theta_vec,omega_vec,omega_mean] = circle_map_phase_attractive(omega,K,A,sigma,N,transi,plotting)
%circle_map_phase_attractive     Iterate the phase attractive circle map from deGuzman Bio Cyb 1991
%
% [theta_vec,omega_vec,omega_mean] = circle_map_phase_attractive(omega,K,A,sigma,N,transi,plotting)
%
% The parameters are self-explanatory:
%   omega drives
%   K couples
%   A relative strenght of the second term added to the classical circle map. 
%   sigma of the additive Gaussian noise
%   N steps
%   transi, how many steps to remove from the beginning
%   plotting, a flag for visualizing the raw data in various ways
%
% Dobromir Dotov, 2021

transient_init = 1+transi;

% Assume phase-locked trial start.
thetas = zeros(N,2);
omega_vec = zeros(N,1);
if omega<=1
    thetas(1,2) = omega;
else
    thetas(1,2) = 1/omega;
end

% Iterate the map.
for n = 2:N
    thetas(n,1) = thetas(n-1,1); % dummy
    omega_vec(n) = omega - K/2/pi*(1+A*cos(2*pi*mod(thetas(n-1,2), 1)))*sin(2*pi*mod(thetas(n-1,2), 1)) + sigma*randn;
    thetas(n,2) = thetas(n-1,2) + omega_vec(n);
end

%Remove a num of initial steps.
thetas = thetas(transient_init:end,:);
omega_vec = omega_vec(transient_init:end);
theta_vec = thetas(:,2);
omega_mean = mean(omega_vec);

% Various ways of looking at the raw behaviour.
if plotting == 1
    figure(1)
    subplot(3,1,1)
    plot(mod(thetas(:,2),1),'-ok')
    ylim([0 1])
    
    subplot(3,1,2)
    plot(1:size(thetas,1),thetas(:,2),'-ok')
    hold on
    plot(1:size(thetas,1),1+(1:size(thetas,1))*omega,'--','color','r')
    plot(1:size(thetas,1),-1+(1:size(thetas,1))*omega_mean,'--','color','m')
    hold off
    grid on
    
    subplot(3,1,3)
    plot(2:size(thetas(:,2),1),diff(thetas(:,2)),'-ok')
    
    figure(2)
    plot(mod(thetas((1e1+1):end-1,2),1),1e-2+mod(thetas((1e1+1):end-1,2),1),'o','color','b')
    axis([0 1 0 1])
    
    figure(3)
    plot(thetas(round(end-N/10):end,2),thetas(round(end-N/10):end,2).^0*1,'^','color','b')
    set(gca,'XTick',round(thetas(round(end-N/10),2)):1:ceil(thetas(end,2)))
    grid on
    
    figure(4)
    plot(1:size(omega_vec,1),omega_vec*360,'-o','color','b')
    set(gca,'YTick',(0:.25:1)*360)
    grid on
    
    figure(5)
    [cc,nn]=hist(omega_vec*360);
    plot(nn,cc./sum(cc),'-')
    set(gca,'XTick',0:45:360)
    grid on
    
    pause
end
