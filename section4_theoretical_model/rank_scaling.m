function ZM = rank_scaling(X,bins)
%RANK_SCALING     Fit the Z-M rank scaling law and prepare for plotting
% Specify the custom model in zml=@ and then optimize it using Matlab's fit().
% If you're using this for a different task then make sure to check the 
% parameter constraints in fitoptions.
%
% Dobromir Dotov, 2021

FT = make_farey_seq(45);
tree = FT.table;
tree = tree(tree(:,4)<=pi,:);
tree(:,6) = tree(:,2)./tree(:,3)*360;
small_tree = tree(tree(:,5)<=8,:);
if isempty(bins)
    tbins = tree(:,6)';
    db = 1;
    bins = (tbins(1)-50*db):db:tbins(1);
    for i = 2:numel(tbins)
        bins = [bins (tbins(i-1):db:(tbins(i)))];
    end
    bins = [bins tbins(end) (tbins(end):db:(tbins(end)+db*100))];
    bins = unique(bins)./360;
end

% Count per bin. Remember that the specification of the bins is always 
% very important, probably more so than how you fit the curve!
[c,n] = hist(X,bins);
xhist = [n' c'./sum(c)];

% With short trials and sparse distributions on rare occasions you could 
% run in problems, log10(0), so remove the zero-counts.
histtemp = sortrows(xhist(xhist(:,2)>0,:),2,'descend');

% Shortcut. Instead of searching for the corner in the scaling law, only
% fit the left half.
halfpoint = round(size(histtemp,1)/2);

% The simpler law
ranks = (1:halfpoint)';
b = [ranks.^0 log10(ranks)]\(log10(histtemp(1:halfpoint,2)));

% The Zipf-Mandelbrot rank scaling law
zml=@(c,q,s,x)(log10(c)+s*log10(x+q));

v = ver;
if any(strcmp ('Curve Fitting Toolbox', {v.Name}))
    % Fitting params.
    fo = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',[0,  -1, -1e1],...
        'Upper',[ 1e1, 2e1,  1e1],...
        'StartPoint',[.1, 1, 0]);
    ft = fittype(zml,'options',fo);
    [zm_model,g] = fit(ranks,log10(histtemp(1:halfpoint,2)),ft);
    rsquare = g.rsquare;
    fitted_curve = zml(zm_model.c,zm_model.q,zm_model.s,(1:halfpoint)');
    ZM.model = zm_model;
    ZM.zmci = confint(zm_model);
else
    fitted_curve = [ranks.^0 log10(ranks)]*b;
    rsquare = corr(log10(histtemp(1:halfpoint,2)),[ranks.^0 log10(ranks)]*b)^2;
end

ZM.b = b;
ZM.bins = bins;
ZM.fitted_curve = fitted_curve;
ZM.ranks = ranks;
ZM.r2 = rsquare;
ZM.small_tree = small_tree;
ZM.xhist = xhist;