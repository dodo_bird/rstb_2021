function fig_var(level_instr,dv)

if false%true
    [alw,fsz,~,~] = pretty_canvas([4 3]*1.5);
else
    alw=1;fsz=12;
end

plot(level_instr+randn(size(level_instr))./1e2,dv,'o','LineWidth',.5,...
    'color',[.1 .1 .1],'MarkerFaceColor',[.5 .5 .5])

xlabel('Ratio complexity')
ylabel('SD, ⁰')
xlim([min(level_instr)-.5 max(level_instr)+.5])

set(gca,'linewidth',alw)
set(gca,'fontsize',fsz)
