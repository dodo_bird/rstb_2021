function fig_rank_scaling(ZM)

xticks = ZM.small_tree(:,2)./ZM.small_tree(:,3);
p = ZM.small_tree(:,2);
q = ZM.small_tree(:,3);

if false
    if isfield(ZM,'model')
        fprintf('P ~ c(rank+q)^s, (c=%.3f, q=%.3f, s=%.3f, R^2=%.3f)\n', ...
            ZM.model.c, ZM.model.q, ZM.model.s, ZM.r2)
    end
end

xhist = ZM.xhist;
xhist = sortrows(xhist,1,'descend');

if false%true
    [alw,fsz,~,~] = pretty_canvas([4 3]*1.5);
else
    alw=1;fsz=12;
end

plot(xhist(:,1),xhist(:,2),'-','LineWidth',3,'color',[.1 .1 .1])
hold on
plot(xhist(:,1),xhist(:,2),'-s','LineWidth',.1,'color',[.1 .1 .1],...
    'MarkerSize',2,'MarkerFaceColor',[.8 .5 1],'MarkerEdgeColor',[.8 .5 1])
hold off
xlabel('W')
ylabel('P(W)')
grid on
xtickangle(40)
if ~isempty(xticks)
    set(gca,'XTick',xticks)
    xlim([min(xticks)-.1 max(xticks)+.1])
end
if ~isempty(p) && ~isempty(q)
    set(gca,'XTickLabel',[num2str(p) repmat(':',size(p,1),1) num2str(q)])
end
set(gca,'fontsize',fsz)
set(gca,'linewidth',alw)

axes('Position',[.536 .6 .37 .35],'units','normalized')

xhist = sortrows(xhist,2,'descend');
loglog(1:size(xhist,1),xhist(:,2),'s','LineWidth',1,'color',[.1 .3 .1],...
    'MarkerSize',5,'MarkerFaceColor',[.8 .5 1])
hold on
loglog(ZM.ranks,10.^(ZM.fitted_curve),'-.','color',[.1 .85 .4],'LineWidth',4)
hold off
box off
set(gca,'color','none')

if isfield(ZM,'model')
    text(.2,1.05,['$$\tilde P_{Z-M} \propto (k+q)^{' num2str(ZM.model.s,2) '}$$'],...
        'units','normalized','interpreter','latex','fontsize',fsz+5)
end

xlabel('Rank')
ylabel('P(W)')
set(gca,'linewidth',alw)
set(gca,'fontsize',fsz)
